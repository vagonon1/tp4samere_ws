#include <ros/ros.h>
#include <std_msgs/Float64.h>

int main(int argc, char *argv[])
{
  // Definition des 'constantes'
  constexpr int   HZ = 1000; 
  constexpr float PI = 3.14159263;
  
  ros::init(argc,argv,"lidar_rotation");

  ros::NodeHandle node;
  ros::Publisher laposte = node.advertise<std_msgs::Float64>("laser_velocity_controller/command", HZ);
  
  ros::Rate hz(HZ);
  while(ros::ok()) {
    std_msgs::Float64 sms;
    sms.data = PI / 14.0f;

    laposte.publish(sms);

    hz.sleep();
  }
  
  return 0;
}

