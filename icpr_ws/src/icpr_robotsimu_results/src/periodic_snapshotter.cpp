#include <ros/ros.h>
#include <laser_assembler/AssembleScans.h>
#include <sensor_msgs/PointCloud.h>

int main(int argc, char * argv[])
{
	using namespace laser_assembler;
	constexpr int HZ = 1;
	
	ros::init(argc,argv,"periodic_snapshotter");

	ros::NodeHandle node;
  ros::Publisher laposte = node.advertise<sensor_msgs::PointCloud>("/cloud", HZ);

  ros::Rate hz(HZ);

  ros::service::waitForService("assemble_scans");

  ros::ServiceClient sav = node.serviceClient<AssembleScans>("assemble_scans");


 	AssembleScans srv;
  srv.request.end = ros::Time::now();
  srv.request.begin = ros::Time(0,0);


	while(ros::ok()) {
		sensor_msgs::PointCloud sms;

	    if (sav.call(srv)) {
	        sms = srv.response.cloud;
	        laposte.publish(sms);
	    }
  	else {
      printf("Houston, nous avons un problème\n");
  	}
  	
  	hz.sleep();
	}
}
